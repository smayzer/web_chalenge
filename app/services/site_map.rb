require 'fileutils'

class SiteMap
  class Link
    ParseError = Class.new(StandardError)

    attr_accessor :uri, :parent, :deep

    def initialize uri, parent=nil
      return nil if uri.nil?
      self.parent = parent
      self.uri = parse(uri)
      self.deep = parent ? parent.deep + 1 : 1
    end

    def eql?(obj)
      if obj.is_a? self.class
        obj.uri == self.uri
      else
        raise "Can't compare #{ self.class } with #{ obj.class }"
      end
    end

    def hash
      uri.hash
    end

    private

    # TODO add thread to improve performance of parse 
    def parse(uri)
      case uri
      when /^\/{2}/
        URI.parse("http:#{ uri }")
      when /^\//
        raise URI::InvalidURIError, 'invalid link' unless parent
        _uri = parent.uri.clone
        _uri.fragment = nil
        _uri.query = nil
        URI.join(_uri, uri) if uri.size > 1 
      when /^http(|s):\/{2}/
        URI.parse(uri)
      else
        if parent
          _uri = parent.uri.clone
          _uri.fragment = nil
          _uri.query = nil
          URI.join(_uri.to_s, uri)
        else
          URI.parse("http://#{ uri }")
        end
      end
    rescue => e
      raise ParseError, e.message
    end
  end

  attr_accessor :base_link, :deep
  attr_reader :set_of_links

  def initialize(base_link, deep=nil)
    self.set_of_links = Set.new
    self.deep = deep
    self.base_link = Link.new(base_link)
    self.base_link.uri.path = ''
    self.link_queue = [self.base_link]
  end

  def parse
    while link_queue.size > 0
      link = link_queue.shift
      if (deep.nil? || link.deep <= deep) && !set_of_links.include?(link)
        begin
          page = RestClient::Request.execute(:method => :get, :url => link.uri.to_s, :timeout => 5, :open_timeout => 5)
        rescue RestClient::ResourceNotFound => e
          Rails.logger.info("Skip link #{ link.uri.to_s }, #{ e.message }")
        rescue RestClient::RequestTimeout => e
          Rails.logger.info("Timeout request for #{ link.uri.to_s } ")
        end
        parse_link_form_page(page, link)
        set_of_links.add link
      end
    end
    set_of_links
  end

  def to_xml
    xml_builder = Nokogiri::XML::Builder.new(encoding: 'UTF-8') do |xml|
      xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") {
        set_of_links.each do |link|
          xml.url {
            xml.loc_ link.uri.to_s
          }
        end
      }
    end.to_xml
  end

  def to_file
    folder_name = 'site_maps'
    path = Rails.public_path.join(folder_name)
    FileUtils::mkdir_p path
    base_name = base_link.uri.host.gsub('.', '_') + '_' + Time.current.to_i.to_s + '.xml'
    path = path.join base_name
    File.open(path, 'w') { |f| f.write(to_xml) }
    File.join('/', folder_name, base_name)
  end

  private

  attr_accessor :link_queue
  attr_writer :set_of_links

  def parse_link_form_page page, parent_link=nil
    document = Nokogiri::HTML.parse(page)
    links = document.css('a').map do |link| 
      begin
        Link.new(link['href'], parent_link)
      rescue URI::InvalidURIError, Link::ParseError => e
        Rails.logger.info "Parse error for #{ link['href'] }: #{ e.message }"
        nil
      end
    end.compact
    links_for_current_host = links.select { |link| link.uri.try(:host) == base_link.uri.host }
    link_queue.push *links_for_current_host
  end
end