class SiteMapsController < ApplicationController
  def new
  end

  def create
    site_map = SiteMap.new(params[:site_map][:uri], 3)
    site_map.parse
    @file_path = site_map.to_file
    render :show
  rescue SocketError => e
    flash[:alert] = 'Site not available'
    redirect_to root_path
  rescue URI::InvalidURIError, SiteMap::Link::ParseError => e
    flash[:alert] = 'Invalid link'
    redirect_to root_path
  end

  def show
  end
end
